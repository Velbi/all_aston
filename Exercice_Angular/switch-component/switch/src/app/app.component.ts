import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'switch';
  
  constructor() {
    console.log("constructor executed");
  }
  a1 = 0;
  a2 = 1;
  updateA1() {
    console.log(this.a1);
    if(this.a1 == 1){
      document.getElementById("a1").className = "toggler";
      this.a1=0;
    }else{
      document.getElementById("a1").className = "toggler off";
      this.a1=1;
    }
  }
  updateA2() {
    console.log(this.a2)
    if(this.a2 == 1){
      document.getElementById("a2").className = "toggler";
      this.a2=0;
    }else{
      document.getElementById("a2").className = "toggler off";
      this.a2=1;
    }
  }
 
}
