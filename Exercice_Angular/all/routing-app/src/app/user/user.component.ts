import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

const API = "https://jsonplaceholder.typicode.com/users";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  userId: string = "";
  user: any = null;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe((params) => {
        this.userId = params.get("id");
        this.getUser(this.userId);
      })
  }

  getUser(id: string) {
    this.http.get(API + '/' + id)
      .subscribe((res: any) => {
        this.user = res;
      })
  }

}
