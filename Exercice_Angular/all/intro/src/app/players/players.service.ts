import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Players } from './players';

const API = "http://localhost:3000/players";
@Injectable({
  providedIn: 'root'
})


export class PlayersService {
  private players: string[] = ["cerise", "pomme", "kiwi"];

  // Injection du service HttpClient
  constructor(private http: HttpClient) { }

  getPlayers(): string[] {
    return this.players;
  }

  getPlayersAjax() {
    // On retourne l'observable
    // Pas de subscribe à ce niveau
    return this.http.get(API);
  }

  getSuperPlayers(): string {
    return "Grenade";
  }

  postPlayers(player: Players) {
    return this.http.post(API, player);
  }
}
