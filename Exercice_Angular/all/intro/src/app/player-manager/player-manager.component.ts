import { Component, OnInit } from '@angular/core';
import { Player } from './player';
import { PlayerService } from './player.service';

@Component({
  selector: 'app-player-manager',
  templateUrl: './player-manager.component.html',
  styleUrls: ['./player-manager.component.css']
})
export class PlayerManagerComponent implements OnInit {
  private _players: Player[]; // source d'origine inaltérable
  
  players: Player[]; // double pour filtrage et binding avec UI
  player: Player | null = null; // player à modifier
  editMode: boolean = false;

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    this.loadPlayers();
  }

  loadPlayers() {
    this.playerService
      .getAll()
      .subscribe((players: Player[]) => {
        this.players = players;
        this._players = players;
      })
  }

  deletePlayer(event: any) {
    this.playerService.delete(event)
      .subscribe(() => this.loadPlayers())
  }

  editPlayer(event: any) {
    // obtention d'un joueur par filtrage à partir de son id
    this.player = 
      this._players.filter((player: Player) => player.id === event)[0];
    this.editMode = true;
  }
  
  filter(event: any) {
    //exemple: event == { k: "position", option: "défenseur" }
    if (event.option == "") { // aucune option choisie
      this.players = this._players; // retrouve la liste complète
      //this.editMode = false;
    } else {
      this.players = this._players.filter(
        (player: Player) => player[event.k] == event.option)
    }

  }

}
