import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Player, POSITIONS } from '../player';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-player-form',
  templateUrl: './player-form.component.html',
  styleUrls: ['./player-form.component.css']
})
export class PlayerFormComponent implements OnInit {
  positions: string[] = POSITIONS;
  @Input() player: Player | null = null;
  @Input() editMode: boolean = false;
  @Output() onSave: EventEmitter<void> = new EventEmitter();

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    this.initPlayer();
  }

  initPlayer() {
    this.player = {
      lastname: "",
      firstname: "",
      teamid: 1,
      position: "gardien",
      age: 18
    };
    this.editMode = false;
  }

  save() {
    let request;
    if (this.editMode) { // mise à jour d'un joueur
      console.log("** Edit Mode **")
      request = this.playerService.patch(this.player);
    } else { // ajout d'un joueur
      console.log("** Add Mode **")
      request = this.playerService.post(this.player);
    }

    request.subscribe(() => {
      console.log("** Request subscribed **")
      this.onSave.emit();
      this.initPlayer();
    })

  }

}
