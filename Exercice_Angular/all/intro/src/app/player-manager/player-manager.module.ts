import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerManagerComponent } from './player-manager.component';
import { PlayerListComponent } from './player-list/player-list.component';
import { PlayerFormComponent } from './player-form/player-form.component';
import { FormsModule } from '@angular/forms';
import { PlayerFilterComponent } from './player-filter/player-filter.component';

@NgModule({
  declarations: [
    PlayerManagerComponent, 
    PlayerListComponent, 
    PlayerFormComponent, 
    PlayerFilterComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [PlayerManagerComponent]
})
export class PlayerManagerModule { }
