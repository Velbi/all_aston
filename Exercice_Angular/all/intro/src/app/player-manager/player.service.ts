import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Player } from './player';

const API: string = "http://localhost:3000/players";

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(API);
  }

  post(player: Player) {
    return this.http.post(API, player);
  }

  patch(player: Player) {
    return this.http.patch(API + '/' + player.id, player);
  }

  delete(id: number) {
    return this.http.delete(API + '/' + id);
  }
}
