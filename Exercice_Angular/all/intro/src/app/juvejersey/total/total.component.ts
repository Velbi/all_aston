import { Component, OnInit, Input } from '@angular/core';

const UNIT_PRICE: number = 120;
const BADGE_PRICE: number = 10;
const FLOCK_PRICE: number = 10;

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {
  total: number = UNIT_PRICE;
  private _qty: number = 1;
  private _numBadges: number = 0;
  @Input() flock: boolean = false;

  constructor() { }

  ngOnInit(): void {
    console.log("init");
  }

  //https://angular.io/guide/lifecycle-hooks
  ngDoCheck() {
    // recalcule le total dès lors qu'un changement
    // d'état est détecté (propriété modifiée)
    // exemple: ajout de 10 EUR au flocage
    this.computeTotal();
  }

  selectQty(event: any) {
    let qty: number = parseInt(event.target.value);
    this._qty = qty;
    this.computeTotal();
  }

  selectBadge(event: any) {
    let checked: boolean = event.target.checked;
    if (checked) {
      this._numBadges++;
    } else {
      this._numBadges--;
    }
    this.computeTotal();
  }

  private computeTotal() {
    let total = this._qty * UNIT_PRICE;

    if (this._numBadges > 0) 
      total += this._numBadges * BADGE_PRICE;

    if (this.flock) total += FLOCK_PRICE;

    this.total = total;

  }

}
