import { Component, Input, OnInit } from '@angular/core';

const IMAGES_DIR: string = "assets/images/juvejersey/";

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  images: string[] = [];
  @Input() selectedIndex: number = 0;
  @Input() playerName: string = "";
  @Input() playerNum: string = "";

  constructor() { }

  ngOnInit(): void {
    let a = new Array(4).fill(0);
    this.images = a.map((n, i) => IMAGES_DIR + (i+1) + ".jpg");
  }

  selectImage(index: number) {
    this.selectedIndex = index;

    // if (index !== 2) {
    //   this.playerName = "";
    //   this.playerNum = "";
    // }
  }

}
