import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PlayerService } from '../player.service';

@Component({
  selector: 'app-flock',
  templateUrl: './flock.component.html',
  styleUrls: ['./flock.component.css']
})
export class FlockComponent implements OnInit {
  isFlockSelected: boolean = false;
  players: string[] = ["1-toto", "2-zozo", "9-lola"];
  @Output() onFlock: EventEmitter<string> = new EventEmitter();

  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    //this.players = this.playerService.getPlayers();
  }

  selectFlock(value: boolean) {
    this.isFlockSelected = value;

    if (!value) {
      this.onFlock.emit("");
    }
  }

  selectPlayer(event: any) {
    let playerName: string = event.target.value;
    this.onFlock.emit(playerName);
  }

}
