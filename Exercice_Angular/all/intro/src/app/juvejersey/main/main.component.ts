import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  galleryImageIndex: number = 0;
  // playerName: string = "";
  // playerNum: string = "";
  player = { name: "", num: ""};

  constructor() { }

  ngOnInit(): void {
  }

  onFlock(event: any) {
    //console.log(event); // event == "1-Buffon"

    if (event === "-1") { // option = "select a player"
      this.player.name = "";
      this.player.num = "";
    } else {
      let arr = event.split('-');
      this.player.name = arr[0];
      this.player.num = arr[1];
    }

    this.galleryImageIndex = 2; // tshirt de dos
  }

}
