import { Component } from '@angular/core';

interface Image {
  src: string;
  alt: string;
}

interface Student {
  name: string;
  grade: number;
}

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent {
  // propriétés
  title: string = "App Component";
  paragraphText: string = "Simple paragraphe";
  image: Image = {
    src: "https://static.nationalgeographic.fr/files/styles/image_3200/public/01_booktalk_wolves_0.jpg?w=800&h=450",
    alt: "Loup bg"
  };
  students: string[] = ["Noémie", "Umberto", "Thomas"];
  students2: Student[] = [
    { name: "Noémie", grade: 18 },
    { name: "Umberto", grade: 5 },
    { name: "Thomas", grade: 10 }
  ];
  color: string = "green";

  // méthodes
  constructor() {
    console.log("constructor executed");
    //setTimeout(() => this.updateUI(), 3000)
  }

  updateUI() {
    this.title += " updated!";
    this.image.src = "https://remeng.rosselcdn.net/sites/default/files/dpistyles_v2/ena_16_9_extra_big/2019/10/31/node_105291/11408736/public/2019/10/31/B9721440875Z.1_20191031133419_000%2BG8NEQEESK.1-0.jpg?itok=0MrqwGf31572525270";
  }

}
