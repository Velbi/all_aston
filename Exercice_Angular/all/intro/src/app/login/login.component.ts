import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

interface Credentials {
  email: string;
  password: string;
  status?: string;
}

const API = "http://192.168.0.25:8080/lunchtime";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  status: string = "anonymous";
  credentials: Credentials = { 
    email: "toto@gmail.com", password: "bonjour"
  };
  token: string | null = "";

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  login() {
    // TODO: vérfier les inputs
    // Pour accéder à la réponse du serveur à la suite d'une 
    // requête en POST, spécifier { observe: "response" }
    this.http.post(API + "/login", this.credentials, { observe: "response" })
      .subscribe((res: any) => {
        console.log(res.headers.get("Authorization"));
        this.token = res.headers.get("Authorization");
      })
  }

  getUsers() {
    let headers = {
      headers: { "Authorization": this.token }
    };
    // on fournit le token dans les entêtes de la requête
    this.http.get(API + "/user/findall", headers)
      .subscribe((res: any) => {
        console.log(res);
      })
  }

}
