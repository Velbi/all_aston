import { Component } from '@angular/core';
import {interval } from 'rxjs'
interface Image {
  src: string;
  alt: string;
}
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent {
  imageM: Image[] = [{
    src: "https://i.pinimg.com/564x/74/3a/9e/743a9e8d5d22d404c02ea3dc7e95d392.jpg",
    alt: "anima 1"
  },{
    src: "https://i.pinimg.com/564x/fb/21/71/fb217108778527935de26efc903b57a4.jpg",
    alt: "anima 2"
  },{
    src: "https://i.pinimg.com/564x/15/64/a1/1564a174ec0f9ee3a2f08d91154239ef.jpg",
    alt: "anima 3"
  },{
    src: "https://i.pinimg.com/564x/fd/22/a2/fd22a2fd8e55b072054444444d4785fe.jpg",
    alt: "anima 4"
  },{
    src: "https://i.pinimg.com/564x/c1/ba/1c/c1ba1c77e5fb2ccbd91f860c74d534a3.jpg",
    alt: "anima 5"
  }];
  index = 0;
  image: Image = this.imageM[this.index];
  getImage(){
    if (this.index == this.imageM.length) this.index = 0;
    this.image = this.imageM[this.index];
    this.index +=1;
    
  }

  constructor() { 
    interval(3000).subscribe(x => {
      this.getImage();
    });
    
  }  
}
